#region File Description
//-----------------------------------------------------------------------------
// Author: Spencer Haan
// File: Game.cs
//-----------------------------------------------------------------------------
#endregion

using Assignment3.MenuComponents;
using GameStateManagement;
using Microsoft.Xna.Framework;

namespace Assignment3
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        ScreenManager _screenManager;

        public Game()
        {
            Content.RootDirectory = "Content";

            graphics = new GraphicsDeviceManager(this);

            // Set default resolution
            SetResolution(new Vector2(1920, 1080), true);

            // Create the screen manager
            _screenManager = new ScreenManager(this);

            // Add it as a component
            Components.Add(_screenManager);

            // Add the starting screens
            _screenManager.AddScreen(new BackgroundScreen(), null);
            _screenManager.AddScreen(new MainMenuScreen(), null);
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // Call initialize
            base.Initialize();
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Call update
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            // Clear the screen
            graphics.GraphicsDevice.Clear(Color.Black);

            // Call draw
            base.Draw(gameTime);
        }

        public void SetResolution(Vector2 resolution, bool fullScreen)
        {
            // Configure screen
            graphics.PreferredBackBufferWidth = (int)resolution.X;
            graphics.PreferredBackBufferHeight = (int)resolution.Y;
            graphics.IsFullScreen = fullScreen;
            graphics.ApplyChanges();
        }
    }
}
