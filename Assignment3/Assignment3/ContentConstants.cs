﻿#region File Description
//-----------------------------------------------------------------------------
// Author: Spencer Haan
// File: ContentConstants.cs
//-----------------------------------------------------------------------------
#endregion

using System;

namespace Assignment3
{
    /// <summary>
    /// Contains constants for loading content
    /// </summary>
    public class ContentConstants
    {
        // Menu consts
        public const int MenuBackground = 0;
        public const int MenuBlank = 1;
        public const int MenuGradient = 2;
        
        // Ship consts
        public const int ShipPlayer = 3;
        public const int ShipEnemy = 4;
        public const int ShipLaser = 5;
        public const int ShipExahust = 6;

        // Other consts
        public const int MapTiles = 7;
        public const int BorderTiles = 8;
        public const int ResourceBar = 12;

        // Asteroid consts
        public const int AsteroidSmall = 9;
        public const int AsteroidMedium = 10;
        public const int AsteroidLarge = 11;

        // Content names
        public static readonly String[] ContentNames = new String[13] { "images/menu/background", "images/menu/blank", "images/menu/gradient",
            "images/ship/playerShip", "images/ship/enemyShip", "images/ship/laser", "images/ship/engineExhaust",
            "images/mapTileSheet", "images/borderTileSheet", "images/asteroidSmall", "images/asteroidMedium", "images/asteroidLarge", "images/resourceBar" };
    }
}
