﻿#region File Description
//-----------------------------------------------------------------------------
// Author: Spencer Haan
// File: AsteroidSize.cs
//-----------------------------------------------------------------------------
#endregion

namespace Assignment3.AsteroidComponents
{
    /// <summary>
    /// Asteroid size enumerator
    /// </summary>
    public enum AsteroidSize
    {
        Large,
        Medium,
        Small
    }
}
