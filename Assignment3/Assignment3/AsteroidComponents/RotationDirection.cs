﻿#region File Description
//-----------------------------------------------------------------------------
// Author: Spencer Haan
// File: RotationDirection.cs
//-----------------------------------------------------------------------------
#endregion

namespace Assignment3.AsteroidComponents
{
    /// <summary>
    /// Rotation direction enumerator
    /// </summary>
    public enum RotationDirection
    {
        CounterClockwise = -1,
        None,
        Clockwise
    }
}
