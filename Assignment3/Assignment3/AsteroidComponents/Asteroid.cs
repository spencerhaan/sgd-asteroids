﻿#region File Description
//-----------------------------------------------------------------------------
// Author: Spencer Haan
// File: Asteroid.cs
//-----------------------------------------------------------------------------
#endregion

using System;
using Assignment3.GameComponents;
using Assignment3.MapComponents;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Assignment3.AsteroidComponents
{
    public class Asteroid
    {
        // Graphics
        public Texture2D Texture { get; set; }

        // Game settings
        public AsteroidSize Size { get; set; }
        public Resource Health { get; set; }
        public int Damage { get; set; }
        public int Value { get; set; }

        // Desination to draw
        private Rectangle _destination;
        public Rectangle Destination { get { return _destination; } }

        // Center of the asteroid
        public Vector2 Origin { get; set; }

        // Coordinates and positioning
        public MapCoordinate Coordinates;
        public MapPositionerI Positioner { get; set; }
        public Boundaries _boundaries;

        // Game settings
        public float BaseVelocity { get; set; }

        // Current rotation
        private float _rotation;

        // Movement direction
        private float _direction;
        public float Direction
        {
            get { return MathHelper.ToDegrees(_direction); }
            set { _direction = MathHelper.WrapAngle(MathHelper.ToRadians(value)); }
        }

        // Rotation modifiers
        private RotationDirection _rotationDirection;
        private float _rotationSpeed;

        public int Width
        {
            get { return Texture.Width; }
        }
        public int Height
        {
            get { return Texture.Height; }
        }

        public bool Active { get; set; }

        private CollisionProfile _collision;
        public CollisionProfile Collision { get { return _collision; } }

        /// <summary>
        /// Initializes the asteroid
        /// </summary>
        public void Initialize(Texture2D texture, MapCoordinate coordinates, MapPositionerI positioner, Boundaries boundaries,
            float baseVelocity, float direction, RotationDirection rotationDirection, float rotationSpeed, int maxHealth, int damage, int value, AsteroidSize size)
        {
            // The asteroid texture
            Texture = texture;

            // The coordinates, map pasitioner, and boundaries
            Coordinates = coordinates;
            Positioner = positioner;
            _boundaries = boundaries;

            // The origin is the center of the texture
            Origin = new Vector2(Width / 2, Height / 2);

            // Set movement variables
            BaseVelocity = baseVelocity;
            Direction = direction;
            _rotationDirection = rotationDirection;
            _rotationSpeed = rotationSpeed;

            // Set gameplay variables
            Health = new Resource() { ValueMax = maxHealth, Value = maxHealth, Color = Color.Red, Text = "Health" };
            Damage = damage;
            Value = value;
            Size = size;

            // Set collisions
            _collision = new CollisionProfile();
            _collision.Width = Width;
            _collision.Height = Height;
            _collision.PixelData = new Color[Texture.Width * Texture.Height];
            Texture.GetData(_collision.PixelData);

            // Activate the asteroid
            Active = true;
        }

        /// <summary>
        /// Updates the asteroid
        /// </summary>
        public void Update()
        {
            // Apply rotation
            _rotation += (int)_rotationDirection * MathHelper.ToRadians(_rotationSpeed);

            // Calculate the new coordinate position
            float x = BaseVelocity * (float)Math.Cos(_direction);
            float y = BaseVelocity * (float)Math.Sin(_direction);
            Coordinates.Position = new Vector2(Coordinates.Position.X + x, Coordinates.Position.Y + y);

            // Calculate the draw position and set up the destination rectangle
            Vector2 drawPosition = Positioner.GetDrawPosition(Coordinates);
            _destination = new Rectangle((int)drawPosition.X, (int)drawPosition.Y, Width, Height);

            // Determine if the asteroid is still in bounds and set the active flag
            Active = _boundaries.InBounds(new Rectangle(_destination.X - (int)Origin.X, _destination.Y - (int)Origin.Y, Width, Height));

            // Calculate collision profile
            _collision.Transformation = CollisionProfile.Transform(new Vector2(Width / 2, Height / 2), _rotation + MathHelper.PiOver2, drawPosition);
            _collision.Rectangle = CollisionProfile.TransformRectangle(_collision.Transformation, Width, Height);

        }

        /// <summary>
        /// Draws the asteroid
        /// </summary>
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Texture, _destination, new Rectangle(0, 0, Width, Height), Color.White, _rotation, Origin, SpriteEffects.None, 0);
        }
    }
}
