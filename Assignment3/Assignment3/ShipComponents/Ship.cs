﻿#region File Description
//-----------------------------------------------------------------------------
// Author: Spencer Haan
// File: Ship.cs
//-----------------------------------------------------------------------------
#endregion

using System;
using System.Collections.Generic;
using Assignment3.GameComponents;
using Assignment3.MapComponents;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Assignment3.ShipComponents
{
    /// <summary>
    /// Represents a ship that the player utilizes
    /// </summary>
    public class Ship
    {
        // Graphics
        public Texture2D Texture { get; set; }

        // Exhaust particle variables
        public EngineExhaust EngineExhaust;
        private Queue<EngineExhaust> _engineExhaust;

        // Resources
        public Resource Health { get; set; }
        public Resource Lives { get; set; }
        // public Resource EngineResource { get { return Engine.Resource; } }
        // public Resource WeaponResource { get { return Weapon.Resource; } }

        // public ShipEngine Engine { get; set; }
        // public ShipWeapon Weapon { get; set; }

        // Current coordinates and positioning
        public MapCoordinate Coordinates { get; set; }
        public MapPositionerI Positioner { get; set; }

        // Drawing information
        private Rectangle _destination;
        public Rectangle Destination { get { return _destination; } }
        public Vector2 Origin { get; set; }

        // Ship velocity
        public float BaseVelocity { get; set; }

        // Ship acceleration
        public float _acceleration;
        public float Acceleration
        {
            get { return _acceleration; }
            set { _acceleration = MathHelper.Clamp(value, 0, 4); }
        }

        // Ship direction
        private float _direction;
        public float Direction
        {
            get { return MathHelper.ToDegrees(_direction); }
            set { _direction = MathHelper.WrapAngle(MathHelper.ToRadians(value)); }
        }

        public int Width
        {
            get { return Texture.Width; }
        }
        public int Height
        {
            get { return Texture.Height; }
        }

        private CollisionProfile _collision;
        public CollisionProfile Collision { get { return _collision; } }

        /// <summary>
        /// Initializes the ship
        /// </summary>
        public void Initialize(Texture2D texture, Vector2 drawPosition, MapCoordinate coordinates, MapPositionerI positioner, EngineExhaust engineExhaust,
            float baseVelocity, int maxHealth, int maxLives)
        {
            // Texture of the ship
            Texture = texture;

            // Store the coordinates and the map positioner
            Coordinates = coordinates;
            Positioner = positioner;

            // The origin is the center of the texture
            Origin = new Vector2(Width / 2, Height / 2);

            // Create the engine exhaust queue and set the base engine exhaust object
            _engineExhaust = new Queue<EngineExhaust>();
            EngineExhaust = engineExhaust;

            // Set the base velocity and the max and current health
            BaseVelocity = baseVelocity;
            Health = new Resource() { ValueMax = maxHealth, Value = maxHealth, Color = Color.Red, Text = "Health" };
            Lives = new Resource() { ValueMax = maxLives, Value = maxLives, Color = Color.Teal, Text = "Lives" };

            // Create and configure the collision profile
            _collision = new CollisionProfile();
            _collision.Width = Width;
            _collision.Height = Height;
            _collision.PixelData = new Color[Texture.Width * Texture.Height];
            Texture.GetData(_collision.PixelData);
        }
        
        /// <summary>
        /// Updates the ship
        /// </summary>
        public void Update(GameTime gameTime)
        {
            // Remove an old exhaust object
            if (_engineExhaust.Count >= 5)
            {
                _engineExhaust.Dequeue();
            }

            // Calculate the velocity, subtract 1 because exponent of 0 is 1
            float velocity = (float)Math.Pow(BaseVelocity, Acceleration) - 1;

            // Adjust the coordinates
            float x = velocity * (float)Math.Cos(_direction);
            float y = velocity * (float)Math.Sin(_direction);
            Coordinates.Position = new Vector2(Coordinates.Position.X + x, Coordinates.Position.Y + y);

            // Get the current draw position
            Vector2 drawPosition = Positioner.GetDrawPosition(Coordinates);
            _destination = new Rectangle((int)drawPosition.X, (int)drawPosition.Y, Width, Height);

            // Decelerate slightly
            Acceleration -= 0.1f;

            // Add a new exhaust object
            EngineExhaust newEngineExhaust = new EngineExhaust();
            newEngineExhaust.Initialize(EngineExhaust.TileSheet, MapCoordinate.FromCoordinates(Coordinates, false),
                EngineExhaust.Positioner, -80, Direction);
            newEngineExhaust.DecayBase = newEngineExhaust.Width / 5;
            _engineExhaust.Enqueue(newEngineExhaust);

            // Update th engine exhaust
            foreach (EngineExhaust engineExhaust in _engineExhaust)
            {
                engineExhaust.Update();
            }

            // Calculate collision profile
            _collision.Transformation = CollisionProfile.Transform(new Vector2(Width / 2, Height / 2), _direction + MathHelper.PiOver2, drawPosition);
            _collision.Rectangle = CollisionProfile.TransformRectangle(_collision.Transformation, Width, Height);
        }

        /// <summary>
        /// Draws the ship
        /// </summary>
        public void Draw(SpriteBatch spriteBatch)
        {
            // Draw the engine exhaust
            foreach (EngineExhaust exhaust in _engineExhaust)
            {
                exhaust.Draw(spriteBatch);
            }

            // Draw ship
            spriteBatch.Draw(Texture, _destination, new Rectangle(0, 0, Width, Height),
                Color.White, _direction + MathHelper.PiOver2, Origin, SpriteEffects.None, 0);
        }
    }
}
