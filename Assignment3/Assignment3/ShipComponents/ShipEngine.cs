﻿#region File Description
//-----------------------------------------------------------------------------
// Author: Spencer Haan
// File: ShipEngine.cs
//-----------------------------------------------------------------------------
#endregion

using Microsoft.Xna.Framework;
using Assignment3.GameComponents;

namespace Assignment3.ShipComponents
{
    /// <summary>
    /// NOTE: Unused, intended for modular ship components
    /// </summary>
    public class ShipEngine
    {
        public Resource Resource { get; set; }

        public float Acceleration { get; set; }

        public Vector2 Position { get; set; }

        public void Initialize(float acceleration)
        {
            Acceleration = acceleration;
        }

        public void Update()
        {
        }

        public void Draw()
        {
        }
    }
}
