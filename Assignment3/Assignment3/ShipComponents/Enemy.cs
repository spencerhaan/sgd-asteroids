﻿#region File Description
//-----------------------------------------------------------------------------
// Author: Spencer Haan
// File: Enemy.cs
//-----------------------------------------------------------------------------
#endregion

using Assignment3.GameComponents;
using Assignment3.MapComponents;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Assignment3.ShipComponents
{
    /// <summary>
    /// TODO Old enemy code, currently not used!
    /// </summary>
    public class Enemy
    {
        // Graphics
        public Texture2D Texture { get; set; }

        public Resource Health { get; set; }
        public Resource EngineResource { get { return Engine.Resource; } }
        public Resource WeaponResource { get { return Weapon.Resource; } }

        public ShipEngine Engine { get; set; }
        public ShipWeapon Weapon { get; set; }
        public Vector2 Origin { get;set;}

        private Rectangle destinationRect;

        public MapCoordinate Coordinates;
        public MapPositionerI Positioner { get; set; }

        private float _rotation;
        public float Rotation
        {
            get { return MathHelper.ToDegrees(_rotation); }
            set { _rotation = MathHelper.WrapAngle(MathHelper.ToRadians(value)); }
        }

        public int Width
        {
            get { return Texture.Width; }
        }
        public int Height
        {
            get { return Texture.Height; }
        }

        public void Initialize(Texture2D texture, MapCoordinate coordinates)
        {
            Texture = texture;
            Coordinates = coordinates;
            Rotation = 0;
        }

        public void Update()
        {
            Rotation += 10f;
            Vector2 drawPosition = Positioner.GetDrawPosition(Coordinates);
            destinationRect = new Rectangle((int)drawPosition.X - Width / 2, (int)drawPosition.Y - Height / 2, Width, Height);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Texture, destinationRect, new Rectangle(0, 0, Width, Height), Color.White, _rotation, new Vector2(Width / 2, Height / 2), SpriteEffects.None, 0);
        }
    }
}
