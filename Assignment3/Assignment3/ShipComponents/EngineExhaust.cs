﻿#region File Description
//-----------------------------------------------------------------------------
// Author: Spencer Haan
// File: EngineExhaust.cs
//-----------------------------------------------------------------------------
#endregion

using System;
using Assignment3.MapComponents;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Assignment3.ShipComponents
{
    /// <summary>
    /// The engine exhaust is a particle used as the tail of a space ship. It decays
    /// it's width over time.
    /// </summary>
    public class EngineExhaust
    {
        // Tilesheet
        public TileSheet TileSheet { get; set; }
        public int TileId;

        // Coordinates and positioning
        public MapCoordinate Coordinates { get; set; }
        public MapPositionerI Positioner { get; set; }

        // Draw related positions
        private Rectangle _destination;
        public Vector2 Origin { get; set; }

        // Game settings
        public float BaseVelocity { get; set; }

        // Direction of exhaust
        private float _direction;
        public float Direction
        {
            get { return MathHelper.ToDegrees(_direction); }
            set { _direction = MathHelper.WrapAngle(MathHelper.ToRadians(value)); }
        }

        // Dimensions of exhaust
        public int Width
        {
            get { return TileSheet.TileWidth; }
        }
        public int Height
        {
            get { return TileSheet.TileHeight; }
        }

        // Decay base amount and rate
        public float DecayBase { get; set; }
        private float _decayRate { get; set; }

        /// <summary>
        /// Initialize exhaust
        /// </summary>
        public void Initialize(TileSheet tileSheet, MapCoordinate coordinates, MapPositionerI positioner, int spawnDistance, float direction)
        {
            // Set external dependancies
            TileSheet = tileSheet;
            Coordinates = coordinates;
            Positioner = positioner;
            Direction = direction;

            // Decay rate starts at 0 (no decay yet)
            _decayRate = 0;

            // Adjust the coordinates
            float x = spawnDistance * (float)Math.Cos(_direction);
            float y = spawnDistance * (float)Math.Sin(_direction);
            Coordinates.Position = new Vector2(Coordinates.Position.X + x, Coordinates.Position.Y + y);

            // Origin is in the center of the tile/texture
            Origin = new Vector2(Width / 2, Height / 2);
        }

        /// <summary>
        /// Updates the exhaust
        /// </summary>
        public void Update()
        {
            // Determine width based on decay rate and increase rate
            float width = Width - DecayBase * _decayRate;
            _decayRate++;

            // Determine new draw position and set destination
            Vector2 drawPosition = Positioner.GetDrawPosition(Coordinates);
            _destination = new Rectangle((int)drawPosition.X, (int)drawPosition.Y, (int)width, Height);
        }

        /// <summary>
        /// Draws the exhaust
        /// </summary>
        public void Draw(SpriteBatch spriteBatch)
        {
            TileSheet.Draw(spriteBatch, TileId, _destination, Color.White, _direction + MathHelper.PiOver2, Origin, SpriteEffects.None, 0);
        }
    }
}
