﻿#region File Description
//-----------------------------------------------------------------------------
// Author: Spencer Haan
// File: ShipWeapon.cs
//-----------------------------------------------------------------------------
#endregion

using Microsoft.Xna.Framework;
using Assignment3.GameComponents;

namespace Assignment3.ShipComponents
{
    /// <summary>
    /// NOTE: Unused, intended for modular ship components
    /// </summary>
    public class ShipWeapon
    {
        public Resource Resource { get; set; }

        public float Damage { get; set; }
        public float RateOfFire { get; set; }

        public Vector2 Position { get; set; }

        public void Initialize(float damage, float rateOfFire)
        {
            Damage = damage;
            RateOfFire = rateOfFire;
        }

        public void Update()
        {
        }

        public void Draw()
        {
        }
    }
}
