﻿#region File Description
//-----------------------------------------------------------------------------
// Author: Spencer Haan
// File: LaserProjectile.cs
//-----------------------------------------------------------------------------
#endregion

using System;
using Assignment3.GameComponents;
using Assignment3.MapComponents;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Assignment3.ShipComponents
{
    /// <summary>
    /// A laser projectile fired by a ship to deal damage.
    /// </summary>
    public class LaserProjectile
    {
        // Tile sheet
        public TileSheet TileSheet { get; set; }
        public int TileId { get; set; }

        // Coordinates and positioning
        public MapCoordinate Coordinates { get; set; }
        public MapPositionerI Positioner { get; set; }

        // Draw information
        private Rectangle _destination;
        public Rectangle Destination { get { return _destination; } }
        public Vector2 Origin { get; set; }

        // Projectile velocity
        public float BaseVelocity { get; set; }

        // Projectile direction
        private float _direction;
        public float Direction
        {
            get { return MathHelper.ToDegrees(_direction); }
            set { _direction = MathHelper.WrapAngle(MathHelper.ToRadians(value)); }
        }

        public int Width
        {
            get { return TileSheet.TileWidth; }
        }
        public int Height
        {
            get { return TileSheet.TileHeight; }
        }

        // Projectile damage
        public int Damage { get; set; }

        public bool Active { get; set; }

        private CollisionProfile _collision;
        public CollisionProfile Collision { get { return _collision; } }

        /// <summary>
        /// Initialize the projectile
        /// </summary>
        public void Initialize(TileSheet tileSheet, MapCoordinate coordinates, MapPositionerI positioner, int spawnDistance, float baseVelocity, float direction)
        {
            // Depdancies
            TileSheet = tileSheet;
            Coordinates = coordinates;
            Positioner = positioner;
            BaseVelocity = baseVelocity;
            Direction = direction;

            // Damage is currently fixed
            Damage = 10;

            // Adjust the coordinates
            float x = spawnDistance * (float)Math.Cos(_direction);
            float y = spawnDistance * (float)Math.Sin(_direction);
            Coordinates.Position = new Vector2(Coordinates.Position.X + x, Coordinates.Position.Y + y);

            // Center of texture
            Origin = new Vector2(Width / 2, Height / 2);

            Active = true;

            // Setup collision profile
            _collision = new CollisionProfile();
            _collision.Width = Width;
            _collision.Height = Height;
            _collision.PixelData = TileSheet.GetData(TileId);
        }

        /// <summary>
        /// Update the projectile
        /// </summary>
        public void Update()
        {
            // Adjust the coordinates
            float x = BaseVelocity * (float)Math.Cos(_direction);
            float y = BaseVelocity * (float)Math.Sin(_direction);
            Coordinates.Position = new Vector2(Coordinates.Position.X + x, Coordinates.Position.Y + y);

            Vector2 drawPosition = Positioner.GetDrawPosition(Coordinates);
            _destination = new Rectangle((int)drawPosition.X, (int)drawPosition.Y, Width, Height);

            if (!Positioner.InBounds(_destination))
            {
                Active = false;
            }

            _collision.Transformation = CollisionProfile.Transform(new Vector2(Width / 2, Height / 2), _direction, drawPosition);
            _collision.Rectangle = CollisionProfile.TransformRectangle(_collision.Transformation, Width, Height);
        }

        /// <summary>
        /// Draw the projectile
        /// </summary>
        /// <param name="spriteBatch"></param>
        public void Draw(SpriteBatch spriteBatch)
        {
            TileSheet.Draw(spriteBatch, TileId, _destination, Color.White, _direction,  Origin, SpriteEffects.None, 0);
        }
    }
}
