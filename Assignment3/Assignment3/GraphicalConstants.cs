﻿#region File Description
//-----------------------------------------------------------------------------
// Author: Spencer Haan
// File: GraphicalConstants.cs
//-----------------------------------------------------------------------------
#endregion

namespace Assignment3
{
    /// <summary>
    /// Contains constants for graphical offsets and tile widths/heights
    /// </summary>
    public class GraphicalConstants
    {
        public const int PlayerExhaustOffset = -85;
        public const int ResourceBarTopOffset = 10;
        public const int TextBotOffset = 35;

        public const int ProjectileTileWidth = 48;
        public const int ProjectileTileHeight = 24;

        public const int ExhaustTileWidth = 32;
        public const int ExhaustTileHeight = 16;

        public const int ResourceTileWidth = 256;
        public const int ResourceTileHeight = 32;
    }
}
