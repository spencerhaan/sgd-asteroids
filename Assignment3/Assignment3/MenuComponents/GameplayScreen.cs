#region File Description
//-----------------------------------------------------------------------------
// Author: Spencer Haan
// File: GameplayScreen.cs
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using System.Collections.Generic;
using Assignment3.AsteroidComponents;
using Assignment3.GameComponents;
using Assignment3.MapComponents;
using Assignment3.ShipComponents;
using GameStateManagement;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
#endregion

namespace Assignment3.MenuComponents
{
    /// <summary>
    /// This screen implements the actual game logic. It is just a
    /// placeholder to get the idea across: you'll probably want to
    /// put some more interesting gameplay in here!
    /// </summary>
    class GameplayScreen : GameScreen
    {
        #region Fields

        public ContentManager content;
        private SpriteFont gameFont;

        private Random random = new Random();

        private float pauseAlpha;

        // Boundary variables
        private Boundaries _playerBounds = new Boundaries(-150, -150, 300, 300);
        private Boundaries _visibleBounds;
        private Vector2 _visibleBoundsCenter;
        private Boundaries _spawnBounds;

        // A list of loaded content
        private List<Texture2D> _loadedContent = new List<Texture2D>();

        // The player profile
        private GameProfile _playerProfile;

        // Flags
        private bool _fireProjectile;
        private bool _useHyperdrive;
        private bool _gameOver;
        
        // Asteroid spawn limit
        private int _asteroidSpawnLimit = 10;

        /* Time stamps */
        // Fire rate
        private TimeSpan _fireTime;
        private TimeSpan _previousFireTime;

        // Asteroid spawn rate
        private TimeSpan _asteroidSpawnTime;
        private TimeSpan _previousAsteroidSpawnTime;

        // Hyperdrive cooldown
        private TimeSpan _hyperdriveCooldownTime;
        private TimeSpan _previousHyperdriveCooldownTime;

        // Tile sheets
        private TileSheet _laserSheet;
        private TileSheet _exhaustSheet;
        private TileSheet _resourceSheet;

        // Object collections
        private List<Asteroid> _asteroidList = new List<Asteroid>();
        private List<LaserProjectile> _playerProjectilesList = new List<LaserProjectile>();
        private Queue<EngineExhaust> _playerExhaustQueue = new Queue<EngineExhaust>();
        private List<Explosion> _explosions = new List<Explosion>();

        // Input state handlers
        private KeyboardState currentKeyboardState;
        private GamePadState currentGamePadState;

        // UI information
        private ResourceBar _playerHealthBar;
        private ResourceBar _playerLivesBar;
        private int _score;

        // Invulnerability state management
        private bool _blinkToggle;
        private bool _playerInvulnerable;
        private TimeSpan _invulnerableStart;
        private TimeSpan _invulnerableReset;
        private TimeSpan _invulnerableBlinkRate;
        private TimeSpan _previousBlink;

        #endregion

        #region Initialization


        /// <summary>
        /// Constructor.
        /// </summary>
        public GameplayScreen()
        {
            TransitionOnTime = TimeSpan.FromSeconds(1.5);
            TransitionOffTime = TimeSpan.FromSeconds(0.5);

            // Reset score
            _score = 0;
        }

        /// <summary>
        /// Load graphics content for the game.
        /// </summary>
        public override void LoadContent()
        {
            if (content == null)
                content = new ContentManager(ScreenManager.Game.Services, "Content");

            // Load all content
            for (int i = 0; i < ContentConstants.ContentNames.Length; i++)
            {
                _loadedContent.Add(content.Load<Texture2D>(ContentConstants.ContentNames[i]));
            }

            // Initialize player profile
            _playerProfile = new GameProfile();
            _playerProfile.PlayerShip = new Ship();

            // Specify time delays
            _fireTime = TimeSpan.FromSeconds(0.15f);
            _asteroidSpawnTime = TimeSpan.FromSeconds(0);
            _invulnerableReset = TimeSpan.FromSeconds(3f);
            _hyperdriveCooldownTime = TimeSpan.FromSeconds(2f);

            // Define the visible boundaries and center
            _visibleBounds = new Boundaries(0, 0, ScreenManager.GraphicsDevice.Viewport.Bounds.Width, ScreenManager.GraphicsDevice.Viewport.Bounds.Height);
            _visibleBoundsCenter = new Vector2(_visibleBounds.Area.X + _visibleBounds.Area.Width / 2, _visibleBounds.Area.Y + _visibleBounds.Area.Height / 2);

            // Define the spawn boundaries
            _spawnBounds = new Boundaries(_visibleBounds.Area.X - GameConstants.SectorSize, _visibleBounds.Area.Y - GameConstants.SectorSize,
                _visibleBounds.Area.Width + GameConstants.SectorSize * 2, _visibleBounds.Area.Height + GameConstants.SectorSize * 2);

            // Load the gamefont
            gameFont = content.Load<SpriteFont>("images/gamefont");

            // Load content
            LoadTileSheets();
            LoadMap();
            LoadPlayer();

            // Set the starting position for the world
            _playerProfile.World.Coordinates.Position = _playerProfile.PlayerShip.Coordinates.Position;

            // once the load has finished, we use ResetElapsedTime to tell the game's
            // timing mechanism that we have just finished a very long frame, and that
            // it should not try to catch up.
            ScreenManager.Game.ResetElapsedTime();
        }

        private void LoadTileSheets()
        {
            // Create laser tile sheet
            _laserSheet = new TileSheet();
            _laserSheet.Initialize(_loadedContent[ContentConstants.ShipLaser], GraphicalConstants.ProjectileTileWidth, GraphicalConstants.ProjectileTileHeight);

            // Create exhaust tile sheet
            _exhaustSheet = new TileSheet();
            _exhaustSheet.Initialize(_loadedContent[ContentConstants.ShipExahust], GraphicalConstants.ExhaustTileWidth, GraphicalConstants.ExhaustTileHeight);

            // Create resource tile sheet
            _resourceSheet = new TileSheet();
            _resourceSheet.Initialize(_loadedContent[ContentConstants.ResourceBar], GraphicalConstants.ResourceTileWidth, GraphicalConstants.ResourceTileHeight);

        }

        private void LoadMap()
        {
            // Load the map tile sheet
            TileSheet mapSheet = new TileSheet();
            mapSheet.Initialize(_loadedContent[ContentConstants.MapTiles], 256, 256);

            // Load the border tile sheet
            TileSheet borderSheet = new TileSheet();
            borderSheet.Initialize(_loadedContent[ContentConstants.BorderTiles], 256, 256);

            // Create and initialize the world map
            _playerProfile.World = new Map();
            _playerProfile.World.Initialize(mapSheet, borderSheet, _visibleBounds, _visibleBoundsCenter,
                GameConstants.PlayerStartSector, GameConstants.PlayerStartPosition, _playerBounds, GameConstants.SectorSize, GameConstants.SectorSize);
        }

        private void LoadPlayer()
        {
            // The coordinates the ship will be on the map
            MapCoordinate coordinates = new MapCoordinate(GameConstants.SectorSize, GameConstants.SectorSize,
                GameConstants.PlayerStartSector, GameConstants.PlayerStartPosition, true, _playerBounds);

            // Create the engine exhaust
            EngineExhaust exhaust = new EngineExhaust();
            exhaust.Initialize(_exhaustSheet, MapCoordinate.FromCoordinates(coordinates, false),
                _playerProfile.World, GraphicalConstants.PlayerExhaustOffset, _playerProfile.PlayerShip.Direction);

            // Create the ship and initialize it
            _playerProfile.PlayerShip = new Ship();
            _playerProfile.PlayerShip.Initialize(_loadedContent[ContentConstants.ShipPlayer], _visibleBoundsCenter, coordinates, _playerProfile.World, exhaust,
                GameConstants.PlayerShipBaseVelocity, GameConstants.PlayerHealth, GameConstants.PlayerBaseExtraLives);

            // Initialize the health resource bar
            _playerHealthBar = new ResourceBar();
            _playerHealthBar.Initialize(_resourceSheet, gameFont, _playerProfile.PlayerShip.Health,
                new Vector2(_visibleBounds.Area.Right / 2 - _resourceSheet.TileWidth / 2, _visibleBounds.Area.Top + GraphicalConstants.ResourceBarTopOffset));

            // Initialize the lives resource bar
            _playerLivesBar = new ResourceBar();
            _playerLivesBar.Initialize(_resourceSheet, gameFont, _playerProfile.PlayerShip.Lives,
                new Vector2(_visibleBounds.Area.Right - _resourceSheet.TileWidth, _visibleBounds.Area.Top + GraphicalConstants.ResourceBarTopOffset));
        }


        /// <summary>
        /// Unload graphics content used by the game.
        /// </summary>
        public override void UnloadContent()
        {
            content.Unload();
        }


        #endregion

        #region Update and Draw

        private void AddPlayerProjectile(GameTime gameTime)
        {
            if (gameTime.TotalGameTime - _previousFireTime >= _fireTime)
            {
                // Save the previous time
                _previousFireTime = gameTime.TotalGameTime;

                // Create and initialize the projectile
                LaserProjectile projectile = new LaserProjectile();
                projectile.Initialize(_laserSheet, MapCoordinate.FromCoordinates(_playerProfile.PlayerShip.Coordinates, false), _playerProfile.World, 60, 35, _playerProfile.PlayerShip.Direction);

                // Add it to projectile list
                _playerProjectilesList.Add(projectile);
            }
        }

        private void AddAsteroid()
        {
            // The limits of the spawn boundary in a list to randomize
            List<int> xLimit = new List<int>() { _spawnBounds.Area.Left, _spawnBounds.Area.Right };
            List<int> yLimit = new List<int>() { _spawnBounds.Area.Top, _spawnBounds.Area.Bottom };

            // Determine the spawn position
            float positionX = 0;
            float positionY = 0;
            float direction = 0;
            switch (random.Next(0, 4)) // Four possible areas to spawn from (four edges)
            {
                // Case Left
                case 0:
                    positionX = _spawnBounds.Area.Left;
                    positionY = random.Next(_spawnBounds.Area.Top, _spawnBounds.Area.Bottom);
                    direction = random.Next(-45, 45); // 45 degrees and 315 degrees
                    break;

                // Case Right
                case 1:
                    positionX = _spawnBounds.Area.Right;
                    positionY = random.Next(_spawnBounds.Area.Top, _spawnBounds.Area.Bottom);
                    direction = random.Next(180 - 45, 180 + 45); // 135 degrees and 225 degrees
                    break;

                // Case Top
                case 2:
                    positionX = random.Next(_spawnBounds.Area.Left, _spawnBounds.Area.Right);
                    positionY = _spawnBounds.Area.Top;
                    direction = random.Next(90 - 45, 90 + 45); // 45 degrees and 135 degrees
                    break;

                // Case Bottom
                case 3:
                    positionX = random.Next(_spawnBounds.Area.Left, _spawnBounds.Area.Right);
                    positionY = _spawnBounds.Area.Bottom;
                    direction = random.Next(270 - 45, 270 + 45); // 225 degrees and 315 degrees
                    break;
            }

            // Get the coordinates from the screen position
            MapCoordinate coordinates = _playerProfile.World.GetCoordinates(new Vector2(positionX, positionY));

            // Gameplay variables
            int health = GameConstants.LargeAsteroidHealth;
            int damage = GameConstants.LargeAsteroidDamage;
            int value = GameConstants.LargeAsteroidValue;

            // Create and initialize the asteroid
            Asteroid asteroid = new Asteroid();
            asteroid.Initialize(_loadedContent[ContentConstants.AsteroidLarge], coordinates, _playerProfile.World, _spawnBounds,
                random.Next(1, 5), direction, (RotationDirection)random.Next(-1, 2), random.Next(1, 10), health, damage, value, AsteroidSize.Large);
            asteroid.Update();

            // Add it to the list
            _asteroidList.Add(asteroid);
        }

        public void SplitAsteroid(Asteroid asteroid)
        {
            int health = GameConstants.LargeAsteroidHealth;
            int damage = GameConstants.LargeAsteroidDamage;
            int value = GameConstants.LargeAsteroidValue;

            Texture2D texture = asteroid.Texture;
            AsteroidSize size = asteroid.Size;
            switch (size)
            {
                case AsteroidSize.Large:
                    health = GameConstants.MediumAsteroidHealth;
                    damage = GameConstants.MediumAsteroidDamage;
                    value = GameConstants.MediumAsteroidValue;

                    size = AsteroidSize.Medium;
                    texture = _loadedContent[ContentConstants.AsteroidMedium];
                    break;

                case AsteroidSize.Medium:
                    health = GameConstants.SmallAsteroidHealth;
                    damage = GameConstants.SmallAsteroidDamage;
                    value = GameConstants.SmallAsteroidValue;

                    size = AsteroidSize.Small;
                    texture = _loadedContent[ContentConstants.AsteroidSmall];
                    break;

                // Nothing to be done, can't split further so return
                case AsteroidSize.Small:
                    return;
            }

            for (int i = 0; i < GameConstants.AsteroidSplitAmount; i++)
            {
                float velocity = random.Next(1, 5);
                float direction = random.Next(0, 360);
                RotationDirection rotationDirection = (RotationDirection)random.Next((int)RotationDirection.CounterClockwise, (int)RotationDirection.Clockwise);
                float rotationSpeed = random.Next(1, 10);

                // Create and initialize the asteroid
                Asteroid splitAsteroid = new Asteroid();
                splitAsteroid.Initialize(texture, MapCoordinate.FromCoordinates(asteroid.Coordinates, false), _playerProfile.World, _spawnBounds,
                    velocity, direction, rotationDirection, rotationSpeed, health, damage, value, size);
                splitAsteroid.Update();

                // Add it to the list
                _asteroidList.Add(splitAsteroid);
            }
        }

        /// <summary>
        /// Updates the state of the game. This method checks the GameScreen.IsActive
        /// property, so the game will stop updating when the pause menu is active,
        /// or if you tab away to a different application.
        /// </summary>
        public override void Update(GameTime gameTime, bool otherScreenHasFocus, bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, false);

            // Gradually fade in or out depending on whether we are covered by the pause screen.
            if (coveredByOtherScreen)
                pauseAlpha = Math.Min(pauseAlpha + 1f / 32, 1);
            else
                pauseAlpha = Math.Max(pauseAlpha - 1f / 32, 0);

            if (IsActive)
            {
                // Get the current input states
                currentKeyboardState = Keyboard.GetState();
                currentGamePadState = GamePad.GetState(PlayerIndex.One);

                // Update the player
                UpdatePlayer(gameTime);

                // Update projectiles
                UpdateProjectiles();

                // Update the asteroids
                UpdateAsteroids(gameTime);

                // Update collisions
                UpdateCollisions();

                // Update the UI elements
                UpdateUIElements();

                // Update the state of the game
                UpdateGameState(gameTime);
            }
        }

        public void UpdatePlayer(GameTime gameTime)
        {
            // Toggle the players invulvernability state
            if (_playerInvulnerable && (gameTime.TotalGameTime - _invulnerableStart) > _invulnerableReset)
            {
                _playerInvulnerable = false;
                _invulnerableBlinkRate = TimeSpan.FromSeconds(0.0f);
            }

            // Toggle the players blink state when invulnerable
            if (_playerInvulnerable && (gameTime.TotalGameTime - _previousBlink) >= _invulnerableBlinkRate)
            {
                _previousBlink = gameTime.TotalGameTime;
                _blinkToggle = !_blinkToggle;
            }

            // Update the player
            _playerProfile.PlayerShip.Update(gameTime);

            // Move the world based on the players position
            _playerProfile.World.Update(_playerProfile.PlayerShip.Coordinates.Sector,
                _playerProfile.PlayerShip.Coordinates.Position);

            // Change position to a random position
            if (_useHyperdrive && !_playerInvulnerable &&
                gameTime.TotalGameTime - _previousHyperdriveCooldownTime > _hyperdriveCooldownTime)
            {
                _previousHyperdriveCooldownTime = gameTime.TotalGameTime;

                // We just used the hyper drive, disable it again
                _useHyperdrive = false;

                // Select random sector
                _playerProfile.PlayerShip.Coordinates.Sector = new Vector2(random.Next(_playerBounds.Area.Left, _playerBounds.Area.Right),
                    random.Next(_playerBounds.Area.Top, _playerBounds.Area.Bottom));
            }

            // Fire a projectile
            if (_fireProjectile && !_playerInvulnerable)
            {
                _fireProjectile = false;
                AddPlayerProjectile(gameTime);
            }
        }

        private void UpdateProjectiles()
        {
            for (int i = _playerProjectilesList.Count - 1; i >= 0; i--)
            {
                // Remove inactive projectiles
                if (!_playerProjectilesList[i].Active)
                {
                    _playerProjectilesList.RemoveAt(i);
                }
                else
                {
                    _playerProjectilesList[i].Update();
                }
            }
        }

        private void UpdateAsteroids(GameTime gameTime)
        {
            if (_asteroidList.Count < _asteroidSpawnLimit && gameTime.TotalGameTime - _previousAsteroidSpawnTime > _asteroidSpawnTime)
            {
                // Set the previous spawn time
                _previousAsteroidSpawnTime = gameTime.TotalGameTime;

                // Add a new asteroid
                AddAsteroid();
            }

            // Update the asteroids
            for (int i = _asteroidList.Count - 1; i >= 0; i--)
            {
                // Remove inactive asteroids
                if (!_asteroidList[i].Active)
                {
                    if (_asteroidList[i].Health.Value == 0)
                    {
                        Explosion explosion = new Explosion();
                        explosion.Initialize(ScreenManager.Game, _asteroidList[i].Coordinates, _playerProfile.World, _asteroidList[i].BaseVelocity, 0, gameTime.TotalGameTime);
                        _explosions.Add(explosion);
                    }
                    _asteroidList.RemoveAt(i);
                }
                else
                {
                    _asteroidList[i].Update();
                }
            }

            for (int i = _explosions.Count - 1; i >= 0; i--)
            {
                // Remove inactive asteroids
                if (!_explosions[i].Active)
                {
                    _explosions.RemoveAt(i);
                }
                else
                {
                    _explosions[i].Update(gameTime);
                }
            }
        }

        private void UpdateCollisions()
        {
            // We do not need to handle collision when the player is not vulnerable
            if (_playerInvulnerable || _gameOver)
            {
                return;
            }

            // Get the current collision profile
            CollisionProfile playerCollision = _playerProfile.PlayerShip.Collision;

            for (int i = _asteroidList.Count - 1; i >= 0; i--)
            {
                if (playerCollision.Rectangle.Intersects(_asteroidList[i].Collision.Rectangle))
                {
                    if (CollisionProfile.PixelCollision(playerCollision, _asteroidList[i].Collision))
                    {
                        // Apply damage
                        _playerProfile.PlayerShip.Health.Value -= _asteroidList[i].Damage;
                        _asteroidList[i].Health.Value = 0;

#if WINDOWS

                        // Split the asteroid
                        SplitAsteroid(_asteroidList[i]);

#endif

                        // Deactivate the asteroid
                        _asteroidList[i].Active = false;
                    }
                }
                else
                {
                    foreach (LaserProjectile projectile in _playerProjectilesList)
                    {
                        if (projectile.Collision.Rectangle.Intersects(_asteroidList[i].Collision.Rectangle))
                        {
                            if (CollisionProfile.PixelCollision(projectile.Collision, _asteroidList[i].Collision))
                            {
                                // Apply damage and set active state based on remaining health
                                _asteroidList[i].Health.Value -= projectile.Damage;
                                if (_asteroidList[i].Health.Value == 0)
                                {
                                    // Split the asteroid
                                    SplitAsteroid(_asteroidList[i]);

                                    // Add to the score
                                    _score += _asteroidList[i].Value;

                                    // Deactivate the asteroid
                                    _asteroidList[i].Active = false;
                                }

                                // Deactivate the projectile
                                projectile.Active = false;
                            }
                        }
                    }
                }
            }
        }

        public void UpdateUIElements()
        {
            // Update health bar
            _playerHealthBar.Update();

            // Update lives bar
            _playerLivesBar.Update();
        }

        public void UpdateGameState(GameTime gameTime)
        {
            if (_playerProfile.PlayerShip.Lives.Value == 0)
            {
                GameOver();
            }
            else if (_playerProfile.PlayerShip.Health.Value == 0)
            {
                RespawnPlayer(gameTime);
            }
        }

        public void RespawnPlayer(GameTime gameTime)
        {
            // Reduce lives
            _playerProfile.PlayerShip.Lives.Value--;
            _playerProfile.PlayerShip.Health.Value = _playerProfile.PlayerShip.Health.ValueMax;

            // Set up invulvernability
            _blinkToggle = true;
            _playerInvulnerable = true;
            _invulnerableStart = gameTime.TotalGameTime;
            _invulnerableBlinkRate = TimeSpan.FromSeconds(0.2f);
        }

        public void GameOver()
        {
            _gameOver = true;
        }

        /// <summary>
        /// Lets the game respond to player input. Unlike the Update method,
        /// this will only be called when the gameplay screen is active.
        /// </summary>
        public override void HandleInput(InputState input)
        {
            if (input == null)
                throw new ArgumentNullException("input");

            // Look up inputs for the active player profile.
            int playerIndex = (int)ControllingPlayer.Value;

            // Get current input states
            KeyboardState keyboardState = input.CurrentKeyboardStates[playerIndex];
            GamePadState gamePadState = input.CurrentGamePadStates[playerIndex];

            // The game pauses either if the user presses the pause button, or if
            // they unplug the active gamepad. This requires us to keep track of
            // whether a gamepad was ever plugged in, because we don't want to pause
            // on PC if they are playing with a keyboard and have no gamepad at all!
            bool gamePadDisconnected = !gamePadState.IsConnected && input.GamePadWasConnected[playerIndex];

            if (input.IsPauseGame(ControllingPlayer) || gamePadDisconnected)
            {
                ScreenManager.AddScreen(new PauseMenuScreen(), ControllingPlayer);
            }
            else
            {
                // Don't handle any other input while the game is over
                if (_gameOver)
                {
                    return;
                }

                // Accelerate using the trigger
                if (currentGamePadState.Triggers.Left > 0)
                {
                    _playerProfile.PlayerShip.Acceleration += currentGamePadState.Triggers.Left * GameConstants.PlayerAccelerationRate;
                }
                // Accelerate using the W or Up keys
                else if (currentKeyboardState.IsKeyDown(Keys.Up) ||
                    currentKeyboardState.IsKeyDown(Keys.W))
                {
                    _playerProfile.PlayerShip.Acceleration += GameConstants.PlayerAccelerationRate;
                }

                // Turn using the joy stick
                if (currentGamePadState.ThumbSticks.Left.X != 0)
                {
                    _playerProfile.PlayerShip.Direction += currentGamePadState.ThumbSticks.Left.X * GameConstants.PlayerTurnRate;
                }
                else
                {
                    // Turn left using A or Left key
                    if (currentKeyboardState.IsKeyDown(Keys.Left) ||
                        currentKeyboardState.IsKeyDown(Keys.A))
                    {
                        _playerProfile.PlayerShip.Direction -= GameConstants.PlayerTurnRate;
                    }
                    // Turn right using D or Right key
                    else if (currentKeyboardState.IsKeyDown(Keys.Right) ||
                        currentKeyboardState.IsKeyDown(Keys.D))
                    {
                        _playerProfile.PlayerShip.Direction += GameConstants.PlayerTurnRate;
                    }
                }

                // Fire a projectile
                if (currentKeyboardState.IsKeyDown(Keys.Space) ||
                    currentGamePadState.Triggers.Right > 0)
                {
                    _fireProjectile = true;
                }

                // Use hyperdrive
                if (currentKeyboardState.IsKeyDown(Keys.H) ||
                    currentGamePadState.Buttons.A == ButtonState.Pressed)
                {
                    _useHyperdrive = true;
                }
            }
        }


        /// <summary>
        /// Draws the gameplay screen.
        /// </summary>
        public override void Draw(GameTime gameTime)
        {
            // This game has a blue background. Why? Because!
            ScreenManager.GraphicsDevice.Clear(ClearOptions.Target, Color.Black, 0, 0);

            // Our player and enemy are both actually just text strings.
            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;

            // Begin draw
            spriteBatch.Begin();

            // Draw player world
            _playerProfile.World.Draw(spriteBatch, ScreenManager.GraphicsDevice);

            // Draw laser projectiles
            foreach (LaserProjectile projectile in _playerProjectilesList)
            {
                projectile.Draw(spriteBatch);
            }

            // Draw asteroids
            foreach (Asteroid asteroid in _asteroidList)
            {
                asteroid.Draw(spriteBatch);
            }

            // Draw explosions
            foreach (Explosion explosion in _explosions)
            {
                explosion.Draw(spriteBatch);
            }

            // Draw the player ship, make it blink if invulnerable
            if (!_playerInvulnerable || !_blinkToggle)
            {
                _playerProfile.PlayerShip.Draw(spriteBatch);
            }

            // Draw the score
            spriteBatch.DrawString(gameFont, String.Format("Score: {0}", _score),
                new Vector2(_visibleBounds.Area.Left / 2, _visibleBounds.Area.Top), Color.White);

            // Draw the position
            spriteBatch.DrawString(gameFont, String.Format("Sector: <{0}, {1}>", _playerProfile.World.Coordinates.Sector.X, _playerProfile.World.Coordinates.Sector.Y),
                new Vector2(_visibleBounds.Area.Left, _visibleBounds.Area.Bottom - GraphicalConstants.TextBotOffset), Color.White);

            // Draw resource bars
            _playerHealthBar.Draw(spriteBatch);
            _playerLivesBar.Draw(spriteBatch);

            // Display game over if game is over
            if (_gameOver)
            {
                Vector2 gameOverSize = ScreenManager.Font.MeasureString("Game Over!");
                Vector2 gameOverPosition = new Vector2(_visibleBounds.Area.Width / 2, _visibleBounds.Area.Height / 2);
                spriteBatch.DrawString(gameFont, "Game Over!", gameOverPosition, Color.White, 0, new Vector2(gameOverSize.X / 2, gameOverSize.Y / 2), 1.5f, SpriteEffects.None, 0);
            }

            // End draw
            spriteBatch.End();

            // If the game is transitioning on or off, fade it out to black.
            if (TransitionPosition > 0 || pauseAlpha > 0)
            {
                float alpha = MathHelper.Lerp(1f - TransitionAlpha, 1f, pauseAlpha / 2);

                ScreenManager.FadeBackBufferToBlack(alpha);
            }
        }

        #endregion
    }
}
