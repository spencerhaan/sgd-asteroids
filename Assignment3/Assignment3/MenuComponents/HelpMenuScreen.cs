#region File Description
//-----------------------------------------------------------------------------
// Author: Spencer Haan
// File: HelpMenuScreen.cs
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using Microsoft.Xna.Framework;
using System.Collections.Generic;
#endregion

namespace Assignment3.MenuComponents
{
    /// <summary>
    /// The options screen is brought up over the top of the main menu
    /// screen, and gives the user a chance to configure the game
    /// in various hopefully useful ways.
    /// </summary>
    class HelpMenuScreen : MenuScreen
    {
        #region Initialization


        /// <summary>
        /// Constructor.
        /// </summary>
        public HelpMenuScreen()
            : base("Help")
        {
            // Create back option
            MenuEntry back = new MenuEntry("Back");
            back.Selected += OnCancel;
            
            // Add entries to the menu.
            MenuEntries.Add(new MenuEntry("Autohr: Spencer Haan"));
            MenuEntries.Add(new MenuEntry("Course: Simulation and Game Development"));
            MenuEntries.Add(new MenuEntry("Assignment Title: SET Asteroids"));
            MenuEntries.Add(back);
        }

        #endregion
    }
}
