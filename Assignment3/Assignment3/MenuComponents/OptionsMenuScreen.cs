#region File Description
//-----------------------------------------------------------------------------
// OptionsMenuScreen.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using Microsoft.Xna.Framework;
using System.Collections.Generic;
#endregion

namespace Assignment3.MenuComponents
{
    /// <summary>
    /// The options screen is brought up over the top of the main menu
    /// screen, and gives the user a chance to configure the game
    /// in various hopefully useful ways.
    /// </summary>
    class OptionsMenuScreen : MenuScreen
    {
        #region Fields

        private MenuEntry _resolutionMenuEntry;
        private MenuEntry _fullscreenMenuEntry;

        private static int _resolutionsIndex = 4; // Start at 4
        private static List<Vector2> _resolutions = new List<Vector2>()
        {
            new Vector2(1024, 768),
            new Vector2(1280, 720),
            new Vector2(1440, 900),
            new Vector2(1680, 1050),
            new Vector2(1920, 1080)
        };

        private static bool _fullScreen = true;

        #endregion

        #region Initialization


        /// <summary>
        /// Constructor.
        /// </summary>
        public OptionsMenuScreen()
            : base("Options")
        {
            // Create our menu entries.
            _resolutionMenuEntry = new MenuEntry(string.Empty);
            _fullscreenMenuEntry = new MenuEntry(string.Empty);

            SetMenuEntryText();

            MenuEntry back = new MenuEntry("Back");

            // Hook up menu event handlers.
            _resolutionMenuEntry.Selected += ResolutionsMenuEntrySelected;
            _fullscreenMenuEntry.Selected += FullScreenMenuEntrySelected;
            back.Selected += OnCancel;
            
            // Add entries to the menu.
            MenuEntries.Add(_resolutionMenuEntry);
#if WINDOWS
            MenuEntries.Add(_fullscreenMenuEntry);
#endif
            MenuEntries.Add(back);
        }


        /// <summary>
        /// Fills in the latest values for the options screen menu text.
        /// </summary>
        void SetMenuEntryText()
        {
            _resolutionMenuEntry.Text = string.Format("Display Resolution: {0}", _resolutions[_resolutionsIndex].ToString());
            _fullscreenMenuEntry.Text = string.Format("Use Full Screen: {0}", _fullScreen);
        }


        #endregion

        #region Handle Input


        /// <summary>
        /// Event handler for when the Resolutions menu entry is selected.
        /// </summary>
        void ResolutionsMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            _resolutionsIndex++;

            if (_resolutionsIndex >= _resolutions.Count)
                _resolutionsIndex = 0;

            SetMenuEntryText();
        }

        /// <summary>
        /// Event handler for when the Full Screen menu entry is selected.
        /// </summary>
        void FullScreenMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            _fullScreen = !_fullScreen;
            SetMenuEntryText();
        }

        #endregion

        protected override void OnCancel(PlayerIndex playerIndex)
        {
            ((Assignment3.Game)ScreenManager.Game).SetResolution(_resolutions[_resolutionsIndex], _fullScreen);
            base.OnCancel(playerIndex);
        }
    }
}
