﻿#region File Description
//-----------------------------------------------------------------------------
// Author: Spencer Haan
// File: GameConstants.cs
//-----------------------------------------------------------------------------
#endregion

using Microsoft.Xna.Framework;

namespace Assignment3
{
    /// <summary>
    /// Contains constants for game settings
    /// </summary>
    public class GameConstants
    {
        // Size of a sector
        public const int SectorSize = 256;

        /* Player constants */
        // Graphical
        public const int PlayerProjectileTile = 0;

        // Game settings
        public const int PlayerHealth = 250;
        public const int PlayerBaseExtraLives = 3;
        public const float PlayerShipBaseVelocity = 1.9f;
        public const float PlayerAccelerationRate = 0.25f;
        public const float PlayerTurnRate = 3f;

        // Positioning
        public static readonly Vector2 PlayerStartSector = new Vector2(0, 0);
        public static readonly Vector2 PlayerStartPosition = new Vector2(0, 0);

        /* Projectile constants */
        public const int ProjectileDamage = 10;
        public const float ProjectileBaseVelocity = 35;

        /* Asteroid constants */
        public const int AsteroidSplitAmount = 2;

        // Large
        public const int LargeAsteroidHealth = 30;
        public const int LargeAsteroidDamage = 10;
        public const int LargeAsteroidValue = 100;

        // Medium
        public const int MediumAsteroidHealth = 20;
        public const int MediumAsteroidDamage = 5;
        public const int MediumAsteroidValue = 50;

        // Small
        public const int SmallAsteroidHealth = 10;
        public const int SmallAsteroidDamage = 2;
        public const int SmallAsteroidValue = 10;
    }
}
