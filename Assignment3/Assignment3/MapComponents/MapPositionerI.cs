﻿#region File Description
//-----------------------------------------------------------------------------
// Author: Spencer Haan
// File: MapPositionerI.cs
//-----------------------------------------------------------------------------
#endregion

using Microsoft.Xna.Framework;

namespace Assignment3.MapComponents
{
    /// <summary>
    /// A simple interface to abstract the core functionality of a map from
    /// something like a game object that is utilizing it for positioning.
    /// </summary>
    public interface MapPositionerI
    {
        Vector2 GetDrawPosition(MapCoordinate mapCoordinates);
        bool InBounds(Rectangle destination);
    }
}
