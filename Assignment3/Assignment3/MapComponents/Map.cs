﻿#region File Description
//-----------------------------------------------------------------------------
// Author: Spencer Haan
// File: Map.cs
//-----------------------------------------------------------------------------
#endregion

using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Assignment3.MapComponents
{
    /// <summary>
    /// The map represents a 2D game world. It manages the current size and generated areas,
    /// as well as manages what parts of the map are shown in order to be efficient.
    /// </summary>
    public class Map : MapPositionerI
    {
        // Tilesheets
        public TileSheet MapTileSheet { get; set; }
        public TileSheet BorderTileSheet { get; set; }

        // The position to draw the map at
        public Vector2 DrawPosition { get; set; }

        // Sector storage
        private Dictionary<Vector2, MapSector> _sectors;
        public Dictionary<Vector2, MapSector> Sectors { get { return _sectors; } }

        // Current sector and coordinates
        public MapCoordinate Coordinates { get; set; }

        // The boundaries to restrict how much is shown
        private Boundaries _visibleBounds;

        // The visible sectors
        private Boundaries _visibleSectors;
        public Boundaries VisibleSectors { get { return _visibleSectors; } }

        /// <summary>
        /// Initializes the map
        /// </summary>
        public void Initialize(TileSheet mapTileSheet, TileSheet borderTileSheet, Boundaries visibleBounds, Vector2 drawPosition,
            Vector2 startSector, Vector2 startPosition, Boundaries playerBounds, int sectorWidth, int sectorHeight)
        {
            // Instantiate the sectors dictionary
            _sectors = new Dictionary<Vector2, MapSector>();

            // Set the tile sheets
            MapTileSheet = mapTileSheet;
            BorderTileSheet = borderTileSheet;

            _visibleSectors = new Boundaries(0, 0, 0, 0);

            _visibleBounds = visibleBounds;//new Rectangle(visibleArea.X + (int)drawPosition.X, visibleArea.Y + (int)drawPosition.Y, visibleArea.Width, visibleArea.Height);
            DrawPosition = drawPosition;

            // Set the curent coordinates and create the first sector
            Coordinates = new MapCoordinate(sectorWidth, sectorHeight, startSector, startPosition, true, playerBounds);
        }

        /// <summary>
        /// Updates the maps position
        /// </summary>
        public void Update(Vector2 sector, Vector2 position)
        {
            Coordinates.ChangePosition(sector, position);
            _visibleSectors = GetVisibleSectors();
        }

        /// <summary>
        /// Generates a new map piece, either a regular map tile or a border.
        /// </summary>
        /// <param name="sectorPosition">The position to generate the tile at</param>
        /// <returns>A new map sector</returns>
        private MapSector GenerateMapPiece(Vector2 sectorPosition)
        {
            if (sectorPosition.X < Coordinates.MinSectorX ||
                sectorPosition.X > Coordinates.MaxSectorX ||
                sectorPosition.Y < Coordinates.MinSectorY ||
                sectorPosition.Y > Coordinates.MaxSectorY)
            {
                // Create a border tile
                return new MapSector() { Position = sectorPosition, TileID = BorderTileSheet.GetRandomTile(2, 3), Border = true };
            }
            else
            {
                // Create a map tile
                return new MapSector() { Position = sectorPosition, TileID = MapTileSheet.GetRandomTile(0, MapTileSheet.TileCount) };
            }
        }

        /// <summary>
        /// Returns the screen draw position based on the current center coordinates,
        /// and the provided coordinates.
        /// </summary>
        /// <param name="coordinate">Coordinates to map to screen</param>
        /// <returns>The screen position</returns>
        public Vector2 GetDrawPosition(MapCoordinate coordinate)
        {
            // Determine the difference in sectors
            float deltaSectorX = coordinate.Sector.X - Coordinates.Sector.X;
            float deltaSectorY = coordinate.Sector.Y - Coordinates.Sector.Y;

            // Determine the difference in position
            float deltaPositionX = coordinate.Position.X - Coordinates.Position.X;
            float deltaPositionY = coordinate.Position.Y - Coordinates.Position.Y;

            // Calculate the relative position of the coordinates
            float relativePositionX = deltaSectorX * Coordinates.MaxPositionX + deltaPositionX;
            float relativePositionY = deltaSectorY * Coordinates.MaxPositionY + deltaPositionY;

            // Get the real position based on the draw position
            float positionX = DrawPosition.X + relativePositionX;
            float positionY = DrawPosition.Y + relativePositionY;

            // Return the position
            return new Vector2(positionX, positionY);
        }

        /// <summary>
        /// Gets coordinates based on a coordinates of the screen (in pixels).
        /// </summary>
        /// <param name="position">The screen position</param>
        /// <returns>The coordinate interpretation</returns>
        public MapCoordinate GetCoordinates(Vector2 position)
        {
            float relativePositionX = position.X - DrawPosition.X;
            float relativePositionY = position.Y - DrawPosition.Y;

            MapCoordinate coordinates = MapCoordinate.FromCoordinates(Coordinates, false);
            coordinates.Position = new Vector2(relativePositionX, relativePositionY);

            return coordinates;
        }

        /// <summary>
        /// Checks if the destination rectangle is within the visible boundaries.
        /// </summary>
        /// <param name="destination">Destination rectangle</param>
        /// <returns>True if inbounds, false otherwise</returns>
        public bool InBounds(Rectangle destination)
        {
            return _visibleBounds.InBounds(destination);
        }

        /// <summary>
        /// Returns a sector to draw based on its coordinates.
        /// Configures the positioning information for drawing.
        /// </summary>
        /// <param name="x">Sector x</param>
        /// <param name="y">Sector y</param>
        /// <returns>Map sector to draw</returns>
        private MapSector GetSectorToDraw(int x, int y)
        {
            // Retrieve or create a new sector
            Vector2 sectorPosition = new Vector2(x, y);
            MapSector sector;
            if (_sectors.ContainsKey(sectorPosition))
            {
                sector = _sectors[sectorPosition];
            }
            else
            {
                sector = GenerateMapPiece(sectorPosition);
                _sectors.Add(sectorPosition, sector);
            }

            // Determine the relative sector position (where the sector is in relation to the current center sector)
            float relativeSectorX = (Coordinates.Sector.X - sector.Position.X) * Coordinates.MaxPositionX + Coordinates.Position.X;
            float relativeSectorY = (Coordinates.Sector.Y - sector.Position.Y) * Coordinates.MaxPositionY + Coordinates.Position.Y;

            // Determine the relative draw positions
            float relativeDrawX = DrawPosition.X - relativeSectorX;
            float relativeDrawY = DrawPosition.Y - relativeSectorY;

            // Set the destination and return the sector
            sector.Destination = new Rectangle((int)relativeDrawX, (int)relativeDrawY, Coordinates.MaxPositionX, Coordinates.MaxPositionY);
            return sector;
        }

        /// <summary>
        /// Returns the visible sectors based on the current center sector
        /// </summary>
        /// <returns>Boundaries for the current visible sectors</returns>
        public Boundaries GetVisibleSectors()
        {
            // Calculate how many sectors the viewport can show at once
            double sectorsAlongX = Math.Ceiling(_visibleBounds.Area.Width / (double)Coordinates.MaxPositionX);
            double sectorsAlongY = Math.Ceiling(_visibleBounds.Area.Height / (double)Coordinates.MaxPositionY);

            // Calculate the number of sectors per side
            double sectorsPerSideX = Math.Ceiling(sectorsAlongX / 2.0);
            double sectorsPerSideY = Math.Ceiling(sectorsAlongY / 2.0);

            // Calculate the starting points of the visible sectors
            double sectorsStartX = Coordinates.Sector.X - sectorsPerSideX;
            double sectorsStartY = Coordinates.Sector.Y - sectorsPerSideY;

            /* Return a rectangle defining the sectors that are visible. We add 1 to the width and height to ensure
             * they are negative numbers due to the way the tiles are drawn. Tiles start at <0, 0> as their position,
             * which means that, relative to the center of the screen, the first tile draws the sectors width and height
             * beyond the maps technical center. The 1 ensures that, as the map is moving positively along the X or
             * Y axis, that there is an extra tile to ensure that the map is seamless.
             */
            return new Boundaries((int)sectorsStartX, (int)sectorsStartY, (int)sectorsPerSideX * 2 + 1, (int)sectorsPerSideY * 2 + 1);
        }

        /// <summary>
        /// Draws the visible areas of the map
        /// </summary>
        public void Draw(SpriteBatch spriteBatch, GraphicsDevice graphicsDevice)
        {
            for (int y = VisibleSectors.Area.Top; y < VisibleSectors.Area.Bottom; y++)
            {
                for (int x = VisibleSectors.Area.Left; x < VisibleSectors.Area.Right; x++)
                {
                    MapSector sector = GetSectorToDraw(x, y);
                    if (sector.Border)
                    {
                        BorderTileSheet.Draw(spriteBatch, sector.TileID, sector.Destination, Color.White);
                    }
                    else
                    {
                        MapTileSheet.Draw(spriteBatch, sector.TileID, sector.Destination, Color.White);
                    }
                }
            }
        }
    }
}
