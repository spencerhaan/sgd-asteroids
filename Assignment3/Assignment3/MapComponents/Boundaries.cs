﻿#region File Description
//-----------------------------------------------------------------------------
// Author: Spencer Haan
// File: Boundaries.cs
//-----------------------------------------------------------------------------
#endregion

using Microsoft.Xna.Framework;

namespace Assignment3.MapComponents
{
    /// <summary>
    /// A boundary object that provides a method for boundary checking
    /// </summary>
    public class Boundaries
    {
        private Rectangle _area;
        public Rectangle Area { get { return _area; } }

        public Boundaries(int x, int y, int width, int height)
        {
            _area = new Rectangle(x, y, width, height);
        }

        /// <summary>
        /// Determines if the provided rectangle is within bounds of rectangle.
        /// </summary>
        /// <param name="rectangle">Rectangle to check</param>
        /// <returns>True if inbounds, false otherwise</returns>
        public bool InBounds(Rectangle rectangle)
        {
            return !(rectangle.Right < Area.Left ||
                rectangle.Left > Area.Right ||
                rectangle.Bottom < Area.Top ||
                rectangle.Top > Area.Bottom);
        }
    }
}
