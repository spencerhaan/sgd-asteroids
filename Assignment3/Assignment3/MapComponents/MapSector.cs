﻿#region File Description
//-----------------------------------------------------------------------------
// Author: Spencer Haan
// File: MapSector.cs
//-----------------------------------------------------------------------------
#endregion


using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Assignment3.MapComponents
{
    /// <summary>
    /// A single map sector. Contains its position, tile id, the destination
    /// rectangle, and a flag of whether or not it is a border sector.
    /// </summary>
    public class MapSector
    {
        // Chunk texture
        public int TileID { get; set; }

        // Position information
        public Vector2 Position { get; set; }
        public Rectangle Destination { get; set; }

        public bool Border { get; set; }

        public override bool Equals(object obj)
        {
            if (obj.GetType() == typeof(MapSector))
            {
                return ((MapSector)obj).Position.Equals(this.Position);
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
