﻿#region File Description
//-----------------------------------------------------------------------------
// Author: Spencer Haan
// File: MapCoordinate.cs
//-----------------------------------------------------------------------------
#endregion

using System;
using Microsoft.Xna.Framework;

namespace Assignment3.MapComponents
{
    /// <summary>
    /// A map coordinate is a new coordinate system used with the map in order to simply and reduce the size of
    /// map numbers. This allows the map to be much bigger than using simple integers/longs. Manages the overflow
    /// of positions in order to appropriately change sectors.
    /// </summary>
    public class MapCoordinate
    {
        private Vector2 _sector;
        public Vector2 Sector
        {
            get { return _sector; }
            set { ChangePosition(value, Position); }
        }

        private Vector2 _position;
        public Vector2 Position
        {
            get { return _position; }
            set { ChangePosition(Sector, value); }
        }

        private bool _restrictSectors;
        public bool RestrictSectors { get { return _restrictSectors; } }

        private Boundaries _restrictions;
        public Boundaries Restrictions { get { return _restrictions; } }

        public int MinSectorX { get { return _restrictions.Area.Left; } }
        public int MaxSectorX { get { return _restrictions.Area.Right; } }

        public int MinSectorY { get { return _restrictions.Area.Top; } }
        public int MaxSectorY { get { return _restrictions.Area.Bottom; } }

        public int MaxPositionX { get; set; }
        public int MaxPositionY { get; set; }

        public MapCoordinate(int maxPositionX, int maxPositionY, Vector2 startSector, Vector2 startPosition)
        {
            // Set the max y and y positions
            MaxPositionX = maxPositionX;
            MaxPositionY = maxPositionY;

            // Set the start sector and position
            ChangePosition(startSector, startPosition);
        }

        public MapCoordinate(int maxPositionX, int maxPositionY, Vector2 startSector, Vector2 startPosition, bool restrictSectors, Boundaries restrictions)
            : this(maxPositionX, maxPositionY, startSector, startPosition)
        {
            // Set boundary flag
            _restrictSectors = restrictSectors;
            _restrictions = restrictions;
        }

        /// <summary>
        /// Changes the current sector and position and handles any necessary overflow/underflow
        /// scenarios as well as keeps the coordinates within bounds.
        /// </summary>
        /// <param name="newSector"></param>
        /// <param name="newPosition"></param>
        public void ChangePosition(Vector2 newSector, Vector2 newPosition)
        {
            // Calculate the new X position and X sector (with sector shift)
            float newPositionX = newPosition.X;
            float sectorShiftX = (float)Math.Floor(newPosition.X / (float)MaxPositionX);
            float newSectorX = newSector.X + sectorShiftX;

            // Determine that X sector is within bounds and modify the X position
            // and X sector accordingly
            if (RestrictSectors && (newSectorX < MinSectorX || newSectorX > MaxSectorX))
            {
                newPositionX = (newSectorX > MaxSectorX) ? MaxPositionX : 0;
                newSectorX = MathHelper.Clamp(newSectorX, MinSectorX, MaxSectorX);
            }
            // Handle X position underflow
            else if (newPosition.X < 0)
            {
                newPositionX += MaxPositionX * Math.Abs(sectorShiftX);       
            }
            // Handle X position overflow
            else if (newPosition.X >= MaxPositionX)
            {
                newPositionX -= MaxPositionX * Math.Abs(sectorShiftX);
            }

            // Calculate the new Y position and Y sector (with sector shift)
            float newPositionY = newPosition.Y;
            float sectorShiftY = (float)Math.Floor(newPosition.Y / (float)MaxPositionY);
            float newSectorY = newSector.Y + sectorShiftY;

            // Determine that Y sector is within bounds and modify the Y position
            // and Y sector accordingly
            if (RestrictSectors && (newSectorY < MinSectorY || newSectorY > MaxSectorY))
            {
                newPositionY = (newSectorY > MaxSectorY) ? MaxPositionY : 0;
                newSectorY = MathHelper.Clamp(newSectorY, MinSectorY, MaxSectorY);
            }
            // Handle Y position underflow
            else if (newPosition.Y < 0)
            {
                newPositionY += MaxPositionY * Math.Abs(sectorShiftY);
            }
            // Handle Y position overflow
            else if (newPosition.Y >= MaxPositionY)
            {
                newPositionY -= MaxPositionY * Math.Abs(sectorShiftY);
            }

            // Set the new sector and position
            _sector = new Vector2(newSectorX, newSectorY);
            _position = new Vector2(newPositionX, newPositionY);
        }

        /// <summary>
        /// Creates a new set of coordinates from an already existing coordinate object.
        /// </summary>
        /// <param name="coordinates">The coordinates to clone</param>
        /// <returns>A cloned map coordinate</returns>
        public static MapCoordinate FromCoordinates(MapCoordinate coordinates)
        {
            return new MapCoordinate(coordinates.MaxPositionX, coordinates.MaxPositionY, new Vector2(coordinates.Sector.X, coordinates.Sector.Y), new Vector2(coordinates.Position.X, coordinates.Position.Y),
                coordinates.RestrictSectors, coordinates.Restrictions);
        }

        /// <summary>
        /// Creates a new set of coordinates from an already existing coordinate object. Option
        /// to maintian restrictions.
        /// </summary>
        /// <param name="coordinates">The coordinates to clone</param>
        /// <param name="maintainRestrictions">True to maintain restrictions, false otherwise</param>
        /// <returns>A cloned map coordinate</returns>
        public static MapCoordinate FromCoordinates(MapCoordinate coordinates, bool maintainRestrictions)
        {
            return new MapCoordinate(coordinates.MaxPositionX, coordinates.MaxPositionY, new Vector2(coordinates.Sector.X, coordinates.Sector.Y), new Vector2(coordinates.Position.X, coordinates.Position.Y),
                maintainRestrictions, coordinates.Restrictions);
        }
    }
}
