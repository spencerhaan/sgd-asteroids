﻿#region File Description
//-----------------------------------------------------------------------------
// Author: Spencer Haan
// File: GameProfile.cs
//-----------------------------------------------------------------------------
#endregion

using Assignment3.ShipComponents;
using Assignment3.MapComponents;

namespace Assignment3.GameComponents
{
    /// <summary>
    /// A simple class intended to act as an abstracted means to save the current game state to file.
    /// Not finished.
    /// </summary>
    public class GameProfile
    {
        public Ship PlayerShip { get; set; }
        public Map World { get; set; }
    }
}
