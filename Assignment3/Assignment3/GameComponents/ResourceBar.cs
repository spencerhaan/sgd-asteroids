﻿#region File Description
//-----------------------------------------------------------------------------
// Author: Spencer Haan
// File: ResourceBar.cs
//-----------------------------------------------------------------------------
#endregion

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Assignment3.GameComponents
{
    /// <summary>
    ///  Takes a resource and draws a resource bar using a tile sheet of 3 layers
    /// </summary>
    public class ResourceBar
    {
        public TileSheet TileSheet { get; set; }
        public SpriteFont Font { get; set; }
        public Resource Resource { get; set; }

        public Vector2 DrawPosition { get; set; }

        public int Width
        {
            get { return TileSheet.TileWidth; }
        }
        public int Height
        {
            get { return TileSheet.TileHeight; }
        }

        private float _colorWidth;

        /// <summary>
        /// Initialize the resource bar
        /// </summary>
        public void Initialize(TileSheet tileSheet, SpriteFont font, Resource resource, Vector2 drawPosition)
        {
            TileSheet = tileSheet;
            Font = font;
            Resource = resource;
            DrawPosition = drawPosition;

            // Determine the current color bar width
            _colorWidth = Resource.Value / (float)Resource.ValueMax * Width;
        }

        /// <summary>
        /// Update the resource bar
        /// </summary>
        public void Update()
        {
            _colorWidth = Resource.Value / (float)Resource.ValueMax * Width;
        }

        /// <summary>
        /// Draw the resource bar
        /// </summary>
        public void Draw(SpriteBatch spriteBatch)
        {
            // Draw the base
            TileSheet.Draw(spriteBatch, 2, new Rectangle((int)DrawPosition.X, (int)DrawPosition.Y, Width, Height), Color.White);

            // Draw the color
            TileSheet.Draw(spriteBatch, 1, new Rectangle((int)DrawPosition.X, (int)DrawPosition.Y, (int)_colorWidth, Height), Resource.Color);

            // Draw the frame
            TileSheet.Draw(spriteBatch, 0, new Rectangle((int)DrawPosition.X, (int)DrawPosition.Y, Width, Height), Color.White);

            // Draw label
            spriteBatch.DrawString(Font, Resource.Text, new Vector2(DrawPosition.X + 5, DrawPosition.Y), Color.White, 0, new Vector2(0, 0), 0.75f, SpriteEffects.None, 0);
        }
    }
}
