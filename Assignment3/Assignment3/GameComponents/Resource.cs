﻿#region File Description
//-----------------------------------------------------------------------------
// Author: Spencer Haan
// File: Resource.cs
//-----------------------------------------------------------------------------
#endregion

using System;
using Microsoft.Xna.Framework;

namespace Assignment3.GameComponents
{
    /// <summary>
    /// A resource, such as health
    /// </summary>
    public class Resource
    {
        private int _value;
        public int Value
        {
            get { return _value; }
            set { _value = (int)MathHelper.Clamp(value, 0, ValueMax); }
        }

        public int ValueMax { get; set; }
        public String Text { get; set; }
        public Color Color { get; set; }
    }
}
