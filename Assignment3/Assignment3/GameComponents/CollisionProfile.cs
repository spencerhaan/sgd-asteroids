﻿#region File Description
//-----------------------------------------------------------------------------
// Author: Spencer Haan
// File: CollisionProfile.cs
//-----------------------------------------------------------------------------
#endregion

using System;
using Microsoft.Xna.Framework;

namespace Assignment3.GameComponents
{
    /// <summary>
    /// Handles 2D pixel based collision as well as rectangle based collection detection.
    /// </summary>
    public struct CollisionProfile
    {
        public Matrix Transformation;
        public Rectangle Rectangle;
        public Color[] PixelData;

        public int Width;
        public int Height;

        public CollisionProfile(Matrix transformation, Rectangle rectangle, Color[] pixelData, int width, int height)
        {
            Transformation = transformation;
            Rectangle = rectangle;
            PixelData = pixelData;

            Width = width;
            Height = height;
        }

        public static bool PixelCollision(CollisionProfile a, CollisionProfile b)
        {
            // set A transformation relative to B. B remains at x=0, y=0.
            Matrix AtoB = a.Transformation * Matrix.Invert(b.Transformation);

            // generate a perpendicular vectors to each rectangle side
            Vector2 columnStep, rowStep, rowStartPosition;

            columnStep = Vector2.TransformNormal(Vector2.UnitX, AtoB);
            rowStep = Vector2.TransformNormal(Vector2.UnitY, AtoB);

            // calculate the top left corner of A
            rowStartPosition = Vector2.Transform(Vector2.Zero, AtoB);

            // search each row of pixels in A. start at top and move down.
            for (int rowA = 0; rowA < a.Height; rowA++)
            {
                // begin at the left
                Vector2 pixelPositionA = rowStartPosition;

                // for each column in the row (move left to right)
                for (int colA = 0; colA < a.Width; colA++)
                {
                    // get the pixel position
                    int X = (int)Math.Round(pixelPositionA.X);
                    int Y = (int)Math.Round(pixelPositionA.Y);

                    // if the pixel is within the bounds of B
                    if (X >= 0 && X < b.Width && Y >= 0 && Y < b.Height)
                    {
                        // get colors of overlapping pixels
                        Color colorA = a.PixelData[colA + rowA * a.Width];
                        Color colorB = b.PixelData[X + Y * b.Width];

                        // if both pixels are not completely transparent,
                        if (colorA.A != 0 && colorB.A != 0)
                            return true; // collision
                    }
                    // move to the next pixel in the row of A
                    pixelPositionA += columnStep;
                }
                // move to the next row of A
                rowStartPosition += rowStep;
            }
            return false; // no collision
        }

        public static Matrix Transform(Vector2 center, float rotation, Vector2 position)
        {
            // move to origin, scale (if desired), rotate, translate
            return Matrix.CreateTranslation(new Vector3(-center, 0.0f)) *
                // add scaling here if you want
                                            Matrix.CreateRotationZ(rotation) *
                                            Matrix.CreateTranslation(new Vector3(position, 0.0f));
        }

        public static Rectangle TransformRectangle(Matrix transform, int width, int height)
        {
            // Get each corner of texture
            Vector2 leftTop = new Vector2(0.0f, 0.0f);
            Vector2 rightTop = new Vector2(width, 0.0f);
            Vector2 leftBottom = new Vector2(0.0f, height);
            Vector2 rightBottom = new Vector2(width, height);

            // Transform each corner
            Vector2.Transform(ref leftTop, ref transform, out leftTop);
            Vector2.Transform(ref rightTop, ref transform, out rightTop);
            Vector2.Transform(ref leftBottom, ref transform, out leftBottom);
            Vector2.Transform(ref rightBottom, ref transform, out rightBottom);

            // Find the minimum and maximum corners
            Vector2 min = Vector2.Min(Vector2.Min(leftTop, rightTop),
            Vector2.Min(leftBottom, rightBottom));
            Vector2 max = Vector2.Max(Vector2.Max(leftTop, rightTop),
            Vector2.Max(leftBottom, rightBottom));

            // Return transformed rectangle
            return new Rectangle((int)min.X, (int)min.Y,
                                 (int)(max.X - min.X), (int)(max.Y - min.Y));
        }
    }
}
