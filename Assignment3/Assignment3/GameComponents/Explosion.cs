﻿#region File Description
//-----------------------------------------------------------------------------
// Author: Spencer Haan
// File: TileSheet.cs
//-----------------------------------------------------------------------------
#endregion

using System;
using Assignment3.MapComponents;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ParticleEffects;

namespace Assignment3.GameComponents
{
    /// <summary>
    /// Wraps the ExplosionParticleSystem in order to manage individual explosions and ensure they
    /// maintain their location relative to the map
    /// </summary>
    public class Explosion
    {
        public ExplosionParticleSystem ExplosionParticle { get; set; }
        public ExplosionSmokeParticleSystem SmokeParticle { get; set; }

        private TimeSpan _started;
        private TimeSpan _decayTime = TimeSpan.FromSeconds(2.5f);

        public MapCoordinate Coordinates;
        public MapPositionerI Positioner { get; set; }

        public float BaseVelocity { get; set; }

        private float _direction;
        public float Direction
        {
            get { return MathHelper.ToDegrees(_direction); }
            set { _direction = MathHelper.WrapAngle(MathHelper.ToRadians(value)); }
        }

        public bool Active { get; set; }

        /// <summary>
        /// Initialize the explosion
        /// </summary>
        public void Initialize(Microsoft.Xna.Framework.Game game, MapCoordinate coordinates, MapPositionerI positioner, float baseVelocity, float direction, TimeSpan started)
        {
            // Dependancies
            Coordinates = coordinates;
            Positioner = positioner;
            BaseVelocity = baseVelocity;
            Direction = direction;

            // Current sector position based on direction andm movement
            float x = BaseVelocity * (float)Math.Cos(_direction);
            float y = BaseVelocity * (float)Math.Sin(_direction);
            Coordinates.Position = new Vector2(Coordinates.Position.X + x, Coordinates.Position.Y + y);

            // The current location of the particles
            Vector2 drawPosition = Positioner.GetDrawPosition(Coordinates);

            // Create the smoke particle
            SmokeParticle = new ExplosionSmokeParticleSystem(game, 1);
            SmokeParticle.Initialize();
            SmokeParticle.AddParticles(drawPosition);

            // Create the explosion particle
            ExplosionParticle = new ExplosionParticleSystem(game, 1);
            ExplosionParticle.Initialize();
            ExplosionParticle.AddParticles(drawPosition);

            Active = true;

            _started = started;
        }

        /// <summary>
        /// Update the explosion
        /// </summary>
        public void Update(GameTime gameTime)
        {
            // Get the current position
            Vector2 drawPosition = Positioner.GetDrawPosition(Coordinates);

            // Update the particles
            SmokeParticle.Update(gameTime, drawPosition);
            ExplosionParticle.Update(gameTime, drawPosition);

            // Deactivate explosion if decay time is finished
            if (gameTime.TotalGameTime - _started > _decayTime)
            {
                Active = false;
            }
        }

        /// <summary>
        /// Draw the explosion
        /// </summary>
   
        public void Draw(SpriteBatch spriteBatch)
        {
            SmokeParticle.Draw(spriteBatch);
            ExplosionParticle.Draw(spriteBatch);
        }
    }
}
