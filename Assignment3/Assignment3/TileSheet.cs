﻿#region File Description
//-----------------------------------------------------------------------------
// Author: Spencer Haan
// File: TileSheet.cs
//-----------------------------------------------------------------------------
#endregion

using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Assignment3
{
    /// <summary>
    /// A tile sheet that takes a single texture and draws single tiles of the texture.
    /// </summary>
    public class TileSheet
    {
        private Texture2D _tileSheet;

        private List<Rectangle> _tiles;

        public int TileCount { get { return _tiles.Count; } }

        // Tile dimensions
        public int TileWidth { get; set; }
        public int TileHeight { get; set; }

        // Sheet dimensions
        public int SheetWidth { get { return _tileSheet.Width; } }
        public int SheetHeight { get { return _tileSheet.Height; } }

        private Random _random;

        public void Initialize(Texture2D tileSheet, int tileWidth, int tileHeight)
        {
            _random = new Random();

            // Set tile sheet variables
            _tileSheet = tileSheet;
            TileWidth = tileWidth;
            TileHeight = tileHeight;

            // Configure tile dimensions
            _tiles = new List<Rectangle>();
            for (int y = 0; y < SheetHeight / TileHeight; y++)
            {
                for (int x = 0; x < SheetWidth / TileWidth; x++)
                {
                    Rectangle tile = new Rectangle(x * TileWidth, y * TileHeight, TileWidth, TileHeight);
                    _tiles.Add(tile);
                }
            }
        }

        /// <summary>
        /// Draws the a specific tile at a given destination
        /// </summary>
        public void Draw(SpriteBatch spriteBatch, int tile, Rectangle destination, Color color, float rotation, Vector2 origin, SpriteEffects effects, float layerDepth)
        {
            spriteBatch.Draw(_tileSheet, destination, _tiles[tile], Color.White, rotation, origin, effects, layerDepth);
        }

        /// <summary>
        /// Draws the a specific tile at a given destination
        /// </summary>
        public void Draw(SpriteBatch spriteBatch, int tile, Rectangle destination, Color color)
        {
            spriteBatch.Draw(_tileSheet, destination, _tiles[tile], color);
        }

        /// <summary>
        /// Returns a random tile
        /// </summary>
        /// <param name="low">The lowest tile ID</param>
        /// <param name="high">The highest tile ID</param>
        /// <returns>A random tile between low and high</returns>
        public int GetRandomTile(int low, int high)
        {
            // Fix values
            if (low < 0) low = 0;
            if (high > TileCount) high = TileCount;

            return _random.Next(low, high);
        }

        /// <summary>
        /// Returns the color data of a given tile.
        /// </summary>
        /// <param name="tile">A tile ID</param>
        /// <returns>Color data array</returns>
        public Color[] GetData(int tile)
        {
            Color[] data = new Color[TileWidth * TileHeight];
            _tileSheet.GetData(0, _tiles[tile], data, 0, TileWidth * TileHeight);
            return data;
        }
    }
}
